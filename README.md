# getprops-plugin

[![Download][bintray-badge]][bintray-link]
[![codecov][codecov-badge]][codecov]
[![MIT licensed][mit-badge]][license]
[![standard-readme compliant][readme-badge]][readme-link]

> Gradle plugin that makes importing properties easier.

Reads specified properties from properties files, system properties, or from the environment
and makes them available in the build


## Install

```
#!groovy

buildscript {
    repositories {
        maven {
            url  "https://dl.bintray.com/mreil/gradle-plugins"
        }
    }
    dependencies {
        classpath 'com.mreil.gradle-plugins:gradle-getprops-plugin:${getpropsVersion}'
    }
}

apply plugin: "com.mreil.getprops"
```


## Usage

```
#!groovy

getprops {
    props "secret"
}
```

will read all properties from a __secret.properties__ file (searched up the directory tree starting from the project 
directory), system properties starting with __secret__, and environment variables starting with __SECRET__.


### Example

#### Basic Usage

If the following is true:

* a file __secret.properties__ exists and has the following contents:

        a=val1
        b=val2

* the environment variable __SECRET_C=val3__ is set.
* the system property __secret.d=val4__ is set.

Then `getprops.props.secret` will contain:

```
#!groovy

{
    a: "val1",
    b: "val2",
    c: "val3",
    d: "val4"
}
```

__Note:__ System properties overwrite properties in files and environment variables overwrite system properties if
they have the same name.

A property whose name matches exactly the prefix defined in `getprops` will be made available under the special key 
`_val` if a collision with other properties needs to be avoided. In the example above, an additional environment
variable __SECRET=top__ would be added to the map as `["_val": "top"]`.


#### toEnv

`toEnv()` can be used to convert a property map into a list of environment variable strings which can then
be passed to an external process.

In the above example

```
#!groovy

getprops.toEnv("secret")
```

will return a list of strings as follows:

```
#!groovy

[
    "SECRET_A=val1",
    "SECRET_B=val2",
    "SECRET_C=val3",
    "SECRET_D=val4"
]
```


### Tasks

#### printProps

Prints all the properties that were found.


## Changes

See [CHANGELOG.md][changelog] for changes.


## Maintainer

[Markus Reil](https://bitbucket.org/mreil-com/)


## Contribute

Please raise issues [here](../../issues).

Feel free to address existing issues and raise a [pull request](../../pull-requests).


## License

[MIT][license]


[gradle]: https://gradle.org/  "gradle.org"
[readme-badge]: https://img.shields.io/badge/readme%20style-standard-brightgreen.svg?style=flat-square
[readme-link]: https://github.com/RichardLitt/standard-readme
[bintray-badge]: https://api.bintray.com/packages/mreil/gradle-plugins/getprops-plugin/images/download.svg
[bintray-link]: https://bintray.com/mreil/gradle-plugins/getprops-plugin/_latestVersion
[codecov-badge]: https://codecov.io/bb/mreil-com/gradle-getprops-plugin/branch/master/graph/badge.svg
[codecov]: https://codecov.io/bb/mreil-com/gradle-getprops-plugin
[mit-badge]: https://img.shields.io/badge/license-MIT-blue.svg
[license]: LICENSE
[changelog]: CHANGELOG.md
