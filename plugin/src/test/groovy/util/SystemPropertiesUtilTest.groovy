package util

import com.mreil.getprops.util.SystemPropertiesUtil
import org.junit.BeforeClass
import spock.lang.Shared
import spock.lang.Specification

class SystemPropertiesUtilTest extends Specification {
    public static final String PREFIX = "myprop"

    @Shared
    private static SystemPropertiesUtil util

    @BeforeClass
    static injectProps() {
        System.setProperty("${PREFIX}.key1", "val1")
        util = new SystemPropertiesUtil()
    }

    def "find property with prefix"() {
        when:
        System.setProperty("A", "val1")
        def found = util.getPropertiesStartingWith(PREFIX)

        then:
        found.size() == 1
        found["myprop.key1"] == "val1"
    }

    def "prefix has dot"() {
        when:
        def found = util.getPropertiesStartingWith("${PREFIX}.")

        then:
        found.size() == 1
        found["myprop.key1"] == "val1"
    }

    def "get undefined property"() {
        when:
        def found = util.getPropertiesStartingWith("adskjfhk4j5h3kjh4kfjhdkjh43jk5h")

        then:
        found.isEmpty()
    }

    def "prefix is prop name"() {
        when:
        System.setProperty(PREFIX, "val")
        def found = util.getPropertiesStartingWith(PREFIX)

        then:
        !found.isEmpty()
        found[PREFIX] == 'val'
    }

}
