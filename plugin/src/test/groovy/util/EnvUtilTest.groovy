package util

import com.mreil.getprops.util.EnvUtil
import org.junit.Before
import spock.lang.Shared
import spock.lang.Specification

class EnvUtilTest extends Specification {
    public static final String PREFIX = "MYPROP"

    @Shared
    private EnvUtil envUtil

    @Shared
    private Map<String, String> env = new HashMap<>()

    @Before
    injectProps() {
        env.put("${PREFIX}_KEY1".toString(), "val1")
        envUtil = new EnvUtil({ -> env })
    }

    def "find var with prefix"() {
        when:
        def found = envUtil.getVariablesStartingWith(PREFIX)

        then:
        found.size() == 1
        found["MYPROP_KEY1"] == "val1"
    }

    def "prefix has dot"() {
        when:
        def found = envUtil.getVariablesStartingWith("${PREFIX}_")

        then:
        found.size() == 1
        found["MYPROP_KEY1"] == "val1"
    }

    def "prefix not followed by underscore"() {
        when:
        def found = envUtil.getVariablesStartingWith(PREFIX.substring(0, PREFIX.length()-2))

        then:
        found.isEmpty()
    }

    def "prefix is whole name"() {
        when:
        env.put(PREFIX, "theval")

        and:
        def found = envUtil.getVariablesStartingWith(PREFIX)

        then:
        found.size() == 2
        found.get(PREFIX) == "theval"
    }

    def "get non-existing variable"() {
        when:
        def found = envUtil.getVariablesStartingWith("sdfwsdrf34534535")

        then:
        found.isEmpty()
    }

    def "get real prop"() {
        when:
        envUtil = new EnvUtil()
        def found = envUtil.getVariablesStartingWith("PATH")

        then:
        !found.isEmpty()
    }
}
