package util

import com.mreil.getprops.util.WalkUpFs
import org.junit.ClassRule
import org.junit.rules.TemporaryFolder
import spock.lang.Shared
import spock.lang.Specification

class WalkUpFsTest extends Specification {
    @Shared
    @ClassRule
    TemporaryFolder tmp = new TemporaryFolder()

    @Shared
    private static WalkUpFs walkUpFs = new WalkUpFs()
    
    def "find file in current dir"() {
        when:
        def found = walkUpFs.findFirst("build.gradle", new File(".").toPath())

        then:
        found.isPresent()
        found.get().toString().endsWith("plugin/build.gradle")
    }

    def "find path in current dir"() {
        when:
        def found = walkUpFs.findFirst("src/main", new File(".").toPath())

        then:
        found.isPresent()
        found.get().toString().endsWith("plugin/src/main")
    }

    def "find file in parent"() {
        when:
        def found = walkUpFs.findFirst("config", new File(".").toPath())

        then:
        found.isPresent()
        found.get().toString().endsWith("config")
    }

    def "find file in absolute"() {
        when:
        tmp.newFile("newfile").write("test")

        and:
        def found = walkUpFs.findFirst("newfile", tmp.root.toPath())

        then:
        found.isPresent()
    }

    def "find path in parent"() {
        when:
        def found = walkUpFs.findFirst("config/checkstyle", new File(".").toPath())

        then:
        found.isPresent()
        found.get().toString().endsWith("config/checkstyle")
    }

    def "non-existing file"() {
        when:
        def found = walkUpFs.findFirst("asdfgh456789asdfgh", new File(".").toPath())

        then:
        !found.present
    }

    def "non-existing dir"() {
        when:
        walkUpFs.findFirst("somefile", new File("doesntexist").toPath())

        then:
        thrown IllegalArgumentException
    }
}
