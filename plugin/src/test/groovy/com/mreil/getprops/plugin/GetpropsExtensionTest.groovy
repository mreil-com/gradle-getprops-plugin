package com.mreil.getprops.plugin

import org.gradle.api.Project
import org.gradle.testfixtures.ProjectBuilder
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Specification

class GetpropsExtensionTest extends Specification {
    @Rule
    TemporaryFolder testProjectDir = new TemporaryFolder()

    private GetpropsExtension extension
    private Project project

    @Before
    def setupProject() {
        project = ProjectBuilder.builder()
                .withProjectDir(new File("src/test/resources/testproject1"))
                .withGradleUserHomeDir(testProjectDir.root)
                .build()

        project.plugins.apply(GetpropsPlugin)
        extension = project.extensions.getByName(GetpropsExtension.NAME) as GetpropsExtension
    }

    def "property is read from file"() {
        when:
        project.getprops {
            props "some"
        }

        then:
        extension.props.some.a == "b"
    }

    def "multiple prefixes"() {
        when:
        project.getprops {
            props "some", "other"
        }

        then:
        extension.props.some.a == "b"
        extension.props.other.s == "t"
    }

    def "toEnv"() {
        when:
        project.getprops {
            props "some"
        }

        then:
        extension.toEnv("some") == ["SOME_A=b", "SOME_B=c"]
    }

    def "toEnv multi"() {
        when:
        project.getprops {
            props "some", "other"
        }

        then:
        def env = extension.toEnv("some", "other")
        env.find { it == "SOME_A=b" }
        env.find { it == "OTHER_X=y" }
    }

    def "toEnv all"() {
        when:
        project.getprops {
            props "some", "other"
        }

        then:
        def env = extension.toEnv()
        env.find { it == "SOME_B=c" }
        env.find { it == "OTHER_X=y" }
    }

    def "property is read from env"() {
        when:
        project.getprops {
            props "home"
        }

        then:
        extension.props.home
    }

    def "property is read from system"() {
        when:
        project.getprops {
            props "user"
        }

        then:
        extension.props.user.home
    }

}
