package com.mreil.getprops.plugin

import org.gradle.testkit.runner.BuildResult
import org.gradle.testkit.runner.GradleRunner
import org.gradle.testkit.runner.TaskOutcome
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TemporaryFolder
import spock.lang.Shared
import spock.lang.Specification

import static org.gradle.util.GFileUtils.writeFile

/**
 * Functional test using the GradleRunner
 */
class GetpropsPluginFuncTest extends Specification {
    @Rule
    TemporaryFolder testProjectDir = new TemporaryFolder()

    File buildFile

    @Shared
    GradleRunner gradleRunner

    @Before
    def setupGradleProject() throws IOException {
        buildFile = testProjectDir.newFile("build.gradle")

        String buildFileContent = '''\
plugins {
    id 'com.mreil.getprops'
}

getprops {
    props 'theprop'
}

'''

        writeFile(buildFileContent, buildFile)

        gradleRunner = GradleRunner.create()
                .withPluginClasspath()
                .withProjectDir(testProjectDir.getRoot())
    }

    def "test plugin"() {
        when: "a property is passed in"
        BuildResult result = gradleRunner
                .withArguments(
                [
                        "-Dtheprop.system=mysystemproperty",
                        GetpropsPrintTask.NAME
                ])
                .build()

        then: "it will be discovered"
        result.task(":${GetpropsPrintTask.NAME}").getOutcome() == TaskOutcome.SUCCESS
        result.output.contains("mysystemproperty")
    }
}
