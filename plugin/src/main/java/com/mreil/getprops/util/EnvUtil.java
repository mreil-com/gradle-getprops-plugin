package com.mreil.getprops.util;

import lombok.extern.slf4j.Slf4j;

import java.util.Map;
import java.util.function.Supplier;
import java.util.stream.Collectors;

@Slf4j
public class EnvUtil {
    private final Supplier<Map<String, String>> envSupplier;

    public EnvUtil() {
        this(System::getenv);
    }

    EnvUtil(Supplier<Map<String, String>> envSupplier) {
        this.envSupplier = envSupplier;
    }

    /**
     * Find env variables starting with {@code prefix}.
     * <p>
     * Returns all variables starting with {@code prefix} followed by an underscore and the variable
     * {@code prefix} itself.
     *
     * @param prefix the prefix to search for
     * @return a Map containing all env/value pairs
     */
    public Map<String, String> getVariablesStartingWith(String prefix) {
        Map<String, String> env = getEnv();

        final String prefixWithUnderscore = prefix.endsWith("_") ? prefix : (prefix + "_");

        return env.entrySet().stream()
                .filter(e -> e.getKey().startsWith(prefixWithUnderscore) || e.getKey().equals(prefix))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue
                ));
    }

    private Map<String, String> getEnv() {
        return envSupplier.get();
    }
}
