package com.mreil.getprops.util;

import java.util.HashMap;
import java.util.Map;

/**
 * Util class for merging properties into a nested Map.
 */
public class PropsUtil {

    private PropsUtil() {
    }

    /**
     * Merge flat Map into nested Map of properties.
     *
     * @param props all properties
     * @return a nested Map containing all properties.
     */
    public static Map mergeProps(Map<String, String> props) {
        final Map result = new HashMap();

        props.forEach((key, val) -> merge(result, key, val));

        return result;
    }

    private static void merge(Map map, String key, String val) {
        if (!key.contains(".")) {
            Object existing = map.get(key);
            putExistingUnsafe(map, key, val, existing);
        } else {
            String[] split = key.split("\\.", 2);
            Map sub = getExistingOrNewUnsafe(map, split[0]);
            merge(sub, split[1], val);
        }
    }

    @SuppressWarnings("unchecked")
    private static void putExistingUnsafe(Map map, String key, String val, Object existing) {
        if (existing instanceof Map) {
            ((Map) existing).put("_val", val);
        } else {
            map.put(key, val);
        }
    }

    @SuppressWarnings("unchecked")
    private static Map getExistingOrNewUnsafe(Map map, String key) {
        Object val = map.getOrDefault(key, new HashMap());
        if (val instanceof Map) {
            map.put(key, val);
            return (Map) val;
        } else {
            final Map ret = new HashMap();
            ret.put("_val", val);
            return ret;
        }
    }
}
