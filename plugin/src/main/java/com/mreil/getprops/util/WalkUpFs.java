package com.mreil.getprops.util;

import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.nio.file.Path;
import java.util.Optional;

import static java.util.Optional.empty;

/**
 * Search for a path inside the file system recursively going up the directory tree.
 */
@Slf4j
public class WalkUpFs {
    /**
     * Search for {@code searchFor} starting from {@code in}.
     *
     * @param searchFor the path or file to search for
     * @param in        start searching from this path
     * @return the path if it was found, {@code empty()} otherwise
     */
    public Optional<Path> findFirst(String searchFor, Path in) {
        return findFirst(new File(searchFor).toPath(), in);
    }

    private static Optional<Path> findFirst(Path searchFor, Path in) {
        if (!in.toFile().isDirectory()) {
            throw new IllegalArgumentException("Not a valid directory: " + in);
        }

        if (!in.isAbsolute()) {
            in = new File(System.getProperty("user.dir")).toPath().resolve(in).normalize();
        }

        return findFirstInternal(searchFor, in);
    }

    private static Optional<Path> findFirstInternal(Path searchFor, Path in) {
        log.debug("Trying to find '" + searchFor + "' in '" + in + "'");

        Path resolved = in.resolve(searchFor);
        if (resolved.toFile().exists()) {
            return Optional.of(resolved);
        }

        return findInParent(searchFor, in);
    }

    private static Optional<Path> findInParent(Path searchFor, Path child) {
        File parent = child.toFile().getParentFile();

        if (parent != null && parent.exists()) {
            return findFirstInternal(searchFor, parent.toPath());
        } else {
            return empty();
        }
    }
}
