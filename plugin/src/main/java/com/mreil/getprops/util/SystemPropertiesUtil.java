package com.mreil.getprops.util;

import lombok.extern.slf4j.Slf4j;

import java.util.Collections;
import java.util.Enumeration;
import java.util.Map;
import java.util.Properties;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import static java.util.function.Function.identity;

@Slf4j
public class SystemPropertiesUtil {
    private final Supplier<Map<String, String>> props = new SystemPropertiesSupplier();

    /**
     * Find system properties starting with {@code prefix}.
     * <p>
     * Returns all properties starting with {@code prefix} followed by a dot ('.') and the property called
     * {@code prefix} itself.
     *
     * @param prefix the prefix to search for
     * @return a map containing all property/value pairs
     */
    public Map<String, String> getPropertiesStartingWith(String prefix) {
        Map<String, String> propsList = props.get();

        final String prefixWithDot = prefix.endsWith(".") ? prefix : (prefix + ".");

        return propsList
                .entrySet()
                .stream()
                .filter(entry -> entry.getKey().startsWith(prefixWithDot) || entry.getKey().equals(prefix))
                .collect(Collectors.toMap(
                        Map.Entry::getKey,
                        Map.Entry::getValue
                ));
    }

    private static class SystemPropertiesSupplier implements Supplier<Map<String, String>> {
        @Override
        public Map<String, String> get() {
            Properties props = System.getProperties();
            return Collections.list(getPropertyName(props))
                    .stream()
                    .collect(Collectors.toMap(
                            identity(),
                            props::getProperty
                    ));
        }

        @SuppressWarnings("unchecked")
        private Enumeration<String> getPropertyName(Properties props) {
            return (Enumeration<String>) props.propertyNames();
        }
    }
}
