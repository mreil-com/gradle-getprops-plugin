package com.mreil.getprops.plugin

import org.gradle.api.Plugin
import org.gradle.api.Project

class GetpropsPlugin implements Plugin<Project> {
    @Override
    void apply(Project target) {
        def reader = new PropsReader(target)

        target.extensions.create(GetpropsExtension.NAME,
                GetpropsExtension,
                target,
                reader)

        target.tasks.create(GetpropsPrintTask.NAME, GetpropsPrintTask)
    }
}
