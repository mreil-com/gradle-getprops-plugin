package com.mreil.getprops.plugin

import org.gradle.api.Project

class GetpropsExtension {
    public static final String NAME = "getprops"
    private final PropsReader reader

    def props = [:]

    GetpropsExtension(Project project, PropsReader reader) {
        this.reader = reader
    }

    void props(String... prefixes) {
        for(String prefix: prefixes) {
            def read = this.reader.readProps(prefix)
            read.each { k, v ->
                this.props.put(k, v)
            }
        }
    }

    List<String> toEnv() {
        return toEnv(props.keySet() as String[])
    }

    List<String> toEnv(String... keys) {
        keys.collect{ key ->
            getAll(key, props.get(key))
        }
        .flatten()
    }

    def getAll(String key, def stringOrMap) {
        if (stringOrMap instanceof String) {
            return "${key.toUpperCase()}=${stringOrMap}"
        } else {
            return stringOrMap.collect { k, v ->
                getAll("${key}_${k}", v)
            }.flatten()
        }
    }
}
