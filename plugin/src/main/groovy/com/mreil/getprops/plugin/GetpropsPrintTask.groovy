package com.mreil.getprops.plugin

import org.gradle.api.DefaultTask
import org.gradle.api.logging.LogLevel
import org.gradle.api.tasks.TaskAction

class GetpropsPrintTask extends DefaultTask {
    public static final String NAME = "printProps"

    @TaskAction
    def task() {
        GetpropsExtension extension = project.extensions
                .getByName(GetpropsExtension.NAME) as GetpropsExtension

        getLogger().log(LogLevel.WARN, extension.props.toString())
    }
}
