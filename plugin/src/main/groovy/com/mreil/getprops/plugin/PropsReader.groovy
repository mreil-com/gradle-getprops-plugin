package com.mreil.getprops.plugin

import com.mreil.getprops.util.EnvUtil
import com.mreil.getprops.util.PropsUtil
import com.mreil.getprops.util.SystemPropertiesUtil
import com.mreil.getprops.util.WalkUpFs
import org.gradle.api.Project

import java.nio.file.Path

import static java.util.Collections.emptyMap

class PropsReader {
    private EnvUtil envUtil = new EnvUtil()
    private SystemPropertiesUtil systemPropertiesUtil = new SystemPropertiesUtil()
    private WalkUpFs walkUpFs = new WalkUpFs()

    private final Project project

    PropsReader(Project project) {
        this.project = project
    }

    def readProps(String props) {
        return readProps([props])
    }

    def readProps(List<String> props) {
        Map found = props.collect { key ->
            combine([findPropertiesFile(key, project.projectDir.toPath()),
                     findSystemProps(key),
                     findEnvVars(key)])
        }
        .inject([:]) { acc, val -> acc.putAll(val); acc }
        .collectEntries({String k,v -> [(k.toLowerCase().replace("_", ".")): v]})

        return PropsUtil.mergeProps(found)
    }

    static Map<String, String> combine(List<Map<String, String>> maps) {
        return maps.collect { map -> map.entrySet() }
                .flatten()
                .collectEntries()
    }

    Map<String, String> findPropertiesFile(String prefix, Path searchPath) {
        return walkUpFs
                .findFirst("${prefix}.properties", searchPath)
                .map { path ->
            Properties properties = readPropsFile(path.toFile())
            return properties.stringPropertyNames()
                    .collectEntries {
                k -> [(prefix.toLowerCase() + "." + k): properties.getProperty(k)]
            }
        }
        .orElse(emptyMap())
    }

    static Properties readPropsFile(File file) {
        Properties properties = new Properties()
        file.withInputStream {
            properties.load(it)
        }
        properties
    }

    Map<String, String> findSystemProps(String prefix) {
        return systemPropertiesUtil.getPropertiesStartingWith(prefix.toLowerCase())
    }

    Map<String, String> findEnvVars(String prefix) {
        return envUtil.getVariablesStartingWith(prefix.toUpperCase())
    }
}
