# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [Unreleased]
### Added
- Support multiple arguments for `props()`
- Support converting all properties to env

## [0.1.1] - 2017-10-12
### Added
- `toEnv`

## [0.1.0] - 2017-09-24
### Added
- First released version


[0.1.1]: ../../branches/compare/rel-0.1.1..rel-0.1.0#diff
[0.1.0]: ../../src/rel-0.0.1
[Unreleased]: ../../src
